package br.com.service;

import br.com.entity.GenericEntity;
import br.com.exception.GenericException;

public interface GenericService<T extends GenericEntity> {
	
	T save(T genericEntity) throws GenericException;
	T saveOrUpdate(T genericEntity) throws GenericException;
	void remove(T genericEntity);

}
