package br.com.exception;


public class GenericException extends Exception  {

	public GenericException (){
	    super();
	}

    public GenericException(String message, Throwable throwable) {

        super(message, throwable);
    }

    public GenericException(String message) {

        super(message);
    }

    public GenericException(Throwable throwable) {

        super(throwable);
    }
	

}
