package br.com.dao;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import br.com.entity.GenericEntity;

public interface GenericDAO<T extends GenericEntity> {
     
    T save(T genericEntity) throws SQLIntegrityConstraintViolationException;
    
    void remove(T genericEntity);
 
    T find(T genericEntity);
   
    List<T> findByQuery(String query);

    T saveOrUpdate(T genericEntity);

	T findById(long id);	
	
	List<T> findALL(String ClassName);
   
}