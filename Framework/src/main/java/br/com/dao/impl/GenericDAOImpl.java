package br.com.dao.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.connect.Connect;
import br.com.dao.GenericDAO;
import br.com.entity.GenericEntity;

import java.lang.reflect.ParameterizedType;

@Transactional
@Repository
public class GenericDAOImpl<T extends GenericEntity> implements GenericDAO<T> {

	@SuppressWarnings("unused")
	private Class<T> persistentClass;
	
	@PersistenceContext
    private EntityManager entityManager;
	
	public GenericDAOImpl(){
		super();
	}
	
	public GenericDAOImpl(Class<T> persistentClass) {
        this();
        this.persistentClass = persistentClass;
    }		
	
	
	public T save(T genericEntity) throws org.springframework.dao.DataIntegrityViolationException {
			entityManager.persist(genericEntity);						
        return genericEntity;
        
		
	}

	public void remove(T genericEntity) {		
		entityManager.remove(genericEntity);
			
	}
	public T find(T genericEntity) {
        return genericEntity;
		
	}
	@SuppressWarnings("unchecked")
	public T findById(long id){		
		return (T) entityManager.find(GenericEntity.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findByQuery(String query) {		
		return  entityManager.createQuery(query).getResultList();				
		
	}

	public T saveOrUpdate(T genericEntity) {
		genericEntity = entityManager.merge(genericEntity);
		
        return genericEntity;
	}
	
	public Query createQuery(String query, Object... parameters) {
        Query q = entityManager.createQuery(query);
 
        for (int i = 1; i <= parameters.length; i++) {
            q.setParameter(i, parameters[i]);
        }
         
        return q;
    }
	@SuppressWarnings("unchecked")
    private Class<T> getTypeClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    @SuppressWarnings("unchecked")
	public List<T> findALL(String ClassName) {
		// TODO Auto-generated method stub
		return entityManager.createQuery("FROM " + ClassName).getResultList();
	}

  
}
