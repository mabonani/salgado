package br.com.queuemanager.queuetime.dao.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Named;

import br.com.dao.impl.GenericDAOImpl;
import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queuetime.dao.QueueTimeDAO;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;

@Named
public class QueueTimeDAOImpl extends GenericDAOImpl<QueueTimeEntity> implements QueueTimeDAO {

	@Override
	public List<QueueTimeEntity> findByQueueId(QueueTimeEntity queueTimeEntity) {
		
		String query = "SELECT qt from " + queueTimeEntity.getClass().getName() + " qt " +
				       " WHERE qt.idQueue="+ queueTimeEntity.getIdQueue();
		
		return super.findByQuery(query);					   
		
	}

	@Override
	public List<QueueTimeEntity> findByQueuePasswordCallTime(QueueTimeEntity queueTimeEntity,
			Date startDate, Date endDate) {
		
		String query = "SELECT qt from " + queueTimeEntity.getClass().getName() + " qt " +
			       	   " WHERE qt.idQueue="+ queueTimeEntity.getIdQueue();
		if (startDate != null){
			query += " AND qt.passwordCall >= " + startDate;
		}
		if(endDate != null){
			query += " AND qt.passwordCall <= " + endDate;
		}
		
		return super.findByQuery(query);
	}

	@Override
	public QueueTimeEntity findByQueuePassword(QueueTimeEntity queueTimeEntity) {
		String query = "SELECT qt from " + queueTimeEntity.getClass().getName() + " qt " +
			       " WHERE qt.idQueue="+ queueTimeEntity.getIdQueue() +
		           " AND qt.password =" + queueTimeEntity.getPassword();
		
		List<QueueTimeEntity> queueTimeEntityList = super.findByQuery(query);
		
		if (queueTimeEntityList == null){
			return null;
		}		
		else{
			return queueTimeEntityList.get(0);
		}
	}
	
	

}
