package br.com.queuemanager.queuetime.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.entity.GenericEntity;


@Entity
@Table(name = "QueueTimeEntity")
public class QueueTimeEntity extends GenericEntity {
	
	private static final long serialVersionUID = -8689571258230129733L;	
	
	@Id @GeneratedValue
	@Column(name="idQueueTime", nullable = false)
	private long idQueueTime;
	
	@Column(name="idQueue", nullable = false)
	private long idQueue;
	
	@Column(name="password", nullable =false)
	private Integer password;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="passwordCall", nullable = false)
	private Date passwordCall;

	public long getIdQueueTime() {
		return idQueueTime;
	}

	public void setIdQueueTime(long idQueueTime) {
		this.idQueueTime = idQueueTime;
	}

	public long getIdQueue() {
		return idQueue;
	}

	public void setIdQueue(long idQueue) {
		this.idQueue = idQueue;
	}

	public Integer getPassword() {
		return password;
	}

	public void setPassword(Integer password) {
		this.password = password;
	}
	public Date getPasswordCall() {
		return passwordCall;
	}
	
	public String getPasswordCallTime() {
		return passwordCall.toString();
	}

	public void setPasswordCall(Date passwordCall) {
		this.passwordCall = passwordCall;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
