package br.com.queuemanager.queuetime.restservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queue.exception.QueueException;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;
import br.com.queuemanager.queuetime.exception.QueueTimeException;
import br.com.queuemanager.queuetime.service.QueueTimeService;

@EnableWebMvc
@RestController
public class QueueTimeRestService  {
	
	@Inject
	private QueueTimeService queueTimeService;		
	
	@RequestMapping(method = RequestMethod.GET, value = "/findQueueTimeByPassword")
	public @ResponseBody QueueTimeEntity findQueueTimeByPassword( @RequestParam (value = "idQueue") long idQueue,
																  @RequestParam (value = "password") int password){
		
		QueueTimeEntity queueTimeEntity = new QueueTimeEntity();
		queueTimeEntity.setIdQueue(idQueue);
		queueTimeEntity.setPassword(password);
		
		try {
			return queueTimeService.findByQueuePassword(queueTimeEntity);
		} catch (QueueTimeException e) {
			
			queueTimeEntity = new QueueTimeEntity();
			queueTimeEntity.setMessage(e.getMessage());					
			
			return queueTimeEntity;			
		}
	}
}
