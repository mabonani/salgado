package br.com.queuemanager.queuetime.dao;

import java.util.Date;
import java.util.List;

import br.com.dao.GenericDAO;
import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;

public interface QueueTimeDAO extends GenericDAO<QueueTimeEntity> {
	
	List<QueueTimeEntity> findByQueueId (QueueTimeEntity queueTimeEntity);
	List<QueueTimeEntity> findByQueuePasswordCallTime (QueueTimeEntity queueTimeEntity, Date startDate, Date endDate);
	QueueTimeEntity findByQueuePassword (QueueTimeEntity queueTimeEntity);

}
