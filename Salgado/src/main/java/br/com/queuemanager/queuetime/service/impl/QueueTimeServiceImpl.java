package br.com.queuemanager.queuetime.service.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.exception.GenericException;
import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queuetime.dao.QueueTimeDAO;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;
import br.com.queuemanager.queuetime.exception.QueueTimeException;
import br.com.queuemanager.queuetime.service.QueueTimeService;

@Named
public class QueueTimeServiceImpl implements QueueTimeService {
	
	@Inject
	private QueueTimeDAO queueTimeDAO;

	@Override
	public QueueTimeEntity save(QueueTimeEntity queueTimeEntity)
			throws QueueTimeException {
		
		try {
			return queueTimeDAO.save(queueTimeEntity);
		} catch (SQLIntegrityConstraintViolationException e) {			
			return queueTimeDAO.saveOrUpdate(queueTimeEntity);
		}
	}

	@Override
	public QueueTimeEntity saveOrUpdate(QueueTimeEntity queueTimeEntity)
			throws GenericException {
		
		return queueTimeDAO.saveOrUpdate(queueTimeEntity);
	}

	@Override
	public void remove(QueueTimeEntity queueTimeEntity) {
		queueTimeDAO.remove(queueTimeEntity);		
	}

	@Override
	public List<QueueTimeEntity> findByQueueId(QueueTimeEntity queueTimeEntity) throws QueueTimeException {
		List<QueueTimeEntity> queueTimeEntityList = queueTimeDAO.findByQueueId(queueTimeEntity);
		
		if (queueTimeEntityList == null){
			throw new QueueTimeException("Queue password call time not found!");
		}
		else if (queueTimeEntityList.get(0) == null){
			throw new QueueTimeException("Queue password call time not found!");
		}
		else{
			return queueTimeEntityList;
		}
	}

	@Override
	public List<QueueTimeEntity> findByQueuePasswordCallTime(QueueTimeEntity queueTimeEntity,
			Date startDate, Date endDate) throws QueueTimeException {
		List<QueueTimeEntity> queueTimeEntityList = queueTimeDAO.findByQueuePasswordCallTime(queueTimeEntity, startDate, endDate);
		
		if (queueTimeEntityList == null){
			throw new QueueTimeException("Queue password call time not found!");
		}
		else if (queueTimeEntityList.get(0) == null){
			throw new QueueTimeException("Queue password call time not found!");
		}
		else{
			return queueTimeEntityList;
		}
	}

	@Override
	public QueueTimeEntity findByQueuePassword(QueueTimeEntity queueTimeEntity) throws QueueTimeException {
		queueTimeEntity= queueTimeDAO.findByQueuePassword(queueTimeEntity);
		
		if (queueTimeEntity== null){
			throw new QueueTimeException("Queue password call time not found!");
		}		
		else{
			return queueTimeEntity;
		}
	}

}
