package br.com.queuemanager.queuetime.exception;

import br.com.exception.GenericException;

public class QueueTimeException extends GenericException {
	
	private static final long serialVersionUID = -7312594829641126903L;
	
	public QueueTimeException (){
	    super();
	}

    public QueueTimeException(String message, Throwable throwable) {

        super(message, throwable);
    }

    public QueueTimeException(String message) {

        super(message);
    }

    public QueueTimeException(Throwable throwable) {

        super(throwable);
    }

}
