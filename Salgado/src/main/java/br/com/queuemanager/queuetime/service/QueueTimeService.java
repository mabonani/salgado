package br.com.queuemanager.queuetime.service;

import java.util.Date;
import java.util.List;

import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;
import br.com.queuemanager.queuetime.exception.QueueTimeException;
import br.com.service.GenericService;

public interface QueueTimeService extends GenericService<QueueTimeEntity> {
	List<QueueTimeEntity> findByQueueId (QueueTimeEntity queueTimeEntity) throws QueueTimeException;
	List<QueueTimeEntity> findByQueuePasswordCallTime (QueueTimeEntity queueTimeEntity, Date startDate, Date endDate) throws QueueTimeException;
	QueueTimeEntity findByQueuePassword (QueueTimeEntity queueTimeEntity) throws QueueTimeException;
	QueueTimeEntity save(QueueTimeEntity queueTimeEntity) throws QueueTimeException;
}
