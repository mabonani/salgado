package br.com.queuemanager.queue.dao.impl;

import java.util.List;

import javax.inject.Named;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.dao.impl.GenericDAOImpl;
import br.com.queuemanager.queue.dao.QueueDAO;
import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.user.entity.UserEntity;

@Named
public class QueueDAOImpl extends GenericDAOImpl<QueueEntity> implements QueueDAO {

	@Override
	public QueueEntity findById(QueueEntity queueEntity) {
		return super.findById(queueEntity, queueEntity.getIdQueue());
	}

	@Override
	public List<QueueEntity> findByName(QueueEntity queueEntity) {
		 String query = "SELECT q FROM " + queueEntity.getClass().getName() + " q"  
					+ " WHERE q.name like '%" + queueEntity.getName() + "%'";
		 return super.findByQuery(query);
	}

	@Override
	public List<QueueEntity> findByType(QueueEntity queueEntity) {
		String query = "SELECT q FROM " + queueEntity.getClass().getName() + " q"  
				+ " WHERE q.type like '%" + queueEntity.getType() + "%'";
	 return super.findByQuery(query);
	}	
	
	
	
	

}
