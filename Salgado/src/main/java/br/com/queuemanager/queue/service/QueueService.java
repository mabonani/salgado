package br.com.queuemanager.queue.service;


import java.util.List;

import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queue.exception.QueueException;
import br.com.service.GenericService;

public interface QueueService extends GenericService<QueueEntity> {
	
	QueueEntity  findById (QueueEntity queueEntity) throws QueueException;
	List<QueueEntity> findByName (QueueEntity queueEntity) throws QueueException;
	List<QueueEntity> findByType (QueueEntity queueEntity) throws QueueException;
	QueueEntity saveOrUpdate (QueueEntity queueEntity) throws QueueException;
	QueueEntity save (QueueEntity queueEntity) throws QueueException;
	QueueEntity requestPassword(QueueEntity queueEntity) throws QueueException;
	QueueEntity callNextPassword(QueueEntity queueEntity) throws QueueException;
}
