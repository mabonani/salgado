package br.com.queuemanager.queue.exception;

import br.com.exception.GenericException;

public class QueueException extends GenericException {

	private static final long serialVersionUID = -7603114466524406369L;
	
	public QueueException (){

        super();
    }

    public QueueException(String message, Throwable throwable) {

        super(message, throwable);
    }

    public QueueException(String message) {

        super(message);
    }

    public QueueException(Throwable throwable) {

        super(throwable);
    }

}
