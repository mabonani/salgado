package br.com.queuemanager.queue.service.impl;

import java.util.Date;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.print.attribute.standard.DateTimeAtCompleted;

import br.com.exception.GenericException;
import br.com.queuemanager.queue.dao.QueueDAO;
import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queue.exception.QueueException;
import br.com.queuemanager.queue.service.QueueService;
import br.com.queuemanager.queuetime.entity.QueueTimeEntity;
import br.com.queuemanager.queuetime.exception.QueueTimeException;
import br.com.queuemanager.queuetime.service.QueueTimeService;

@Named
public class QueueServiceImpl implements QueueService {
	
	@Inject
	private QueueDAO queueDAO;
	
	@Inject 
	private QueueTimeService queueTimeService;

	@Override
	public QueueEntity findById(QueueEntity queueEntity) throws QueueException {
		queueEntity = queueDAO.findById(queueEntity);
		
		if(queueEntity == null){
			throw new QueueException("Queue not found");
		}
		
		return queueEntity; 
	}

	@Override
	public List<QueueEntity> findByName(QueueEntity queueEntity) throws QueueException {
		List<QueueEntity> queueList = queueDAO.findByName(queueEntity);
		
		if(queueList == null){
			throw new QueueException("Queue "+ queueEntity.getName() +" not found");
		}
		else if(queueList.size() == 0){
			throw new QueueException("Queue "+ queueEntity.getName() +" not found");
			
		}
		
		return queueList; 
	}

	@Override
	public List<QueueEntity> findByType(QueueEntity queueEntity) throws QueueException {
		List<QueueEntity> queueList = queueDAO.findByType(queueEntity);
		
		if(queueList == null){
			throw new QueueException("Type "+ queueEntity.getType() +" not found");
		}
		
		return queueList; 
	}

	@Override
	public QueueEntity saveOrUpdate(QueueEntity queueEntity) {
		return queueDAO.saveOrUpdate(queueEntity);
	}

	@Override
	public QueueEntity save(QueueEntity queueEntity)  {
		try {
			return queueDAO.save(queueEntity);
		} catch (SQLIntegrityConstraintViolationException e) {
			return queueDAO.saveOrUpdate(queueEntity);
		}
	}

	@Override
	public QueueEntity requestPassword(QueueEntity queueEntity) {
		queueEntity = queueDAO.findById(queueEntity);
		queueEntity.setLastPassword(queueEntity.getLastPassword() + 1);
		this.saveOrUpdate(queueEntity);
		return queueEntity;
		
	}

	@Override
	public QueueEntity callNextPassword(QueueEntity queueEntity) throws QueueException {	
		
		queueEntity = queueDAO.findById(queueEntity);
		queueEntity.setCurrentPassword(queueEntity.getCurrentPassword()+1);			
		
		queueDAO.saveOrUpdate(queueEntity);	
		
		QueueTimeEntity queueTimeEntity = new QueueTimeEntity();
		
		queueTimeEntity.setIdQueue(queueEntity.getIdQueue());
		queueTimeEntity.setPassword(queueEntity.getCurrentPassword());
		queueTimeEntity.setPasswordCall( new Date(System.currentTimeMillis()));
		
		try {
			queueTimeService.save(queueTimeEntity);
		} catch (QueueTimeException e) {			
			throw new QueueException(e.getMessage());
		}	
		
		return queueEntity;
	}

	@Override
	public void remove(QueueEntity genericEntity) {
		// TODO Auto-generated method stub		
	}
	
	

}
