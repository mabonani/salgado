package br.com.queuemanager.queue.dao;

import java.util.List;

import br.com.dao.GenericDAO;
import br.com.queuemanager.queue.entity.QueueEntity;

public interface QueueDAO extends GenericDAO<QueueEntity>{
	
	QueueEntity  findById (QueueEntity queueEntity);
	List<QueueEntity> findByName (QueueEntity queueEntity);
	List<QueueEntity> findByType (QueueEntity queueEntity);
}
