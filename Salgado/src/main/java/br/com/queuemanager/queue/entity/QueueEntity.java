package br.com.queuemanager.queue.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.entity.GenericEntity;

@Entity
@Table(name = "QueueEntity")
public class QueueEntity extends GenericEntity {

	private static final long serialVersionUID = 3393176479055758796L;
	
	@Id @GeneratedValue
	@Column(name="idQueue", nullable = false)
	private long idQueue;
	
	@Column(name="name", nullable = false)
	private String name;
	
	@Column(name="type", nullable = false)
	private String type;	
	
	@Column(name="currentPassword", nullable = true, columnDefinition = "NUMBER default 0")
	private Integer currentPassword;
	
	@Column(name="lastPassword", nullable = true, columnDefinition = "NUMBER default 0")
	private Integer lastPassword;	
	
	@Column(name="averageWait", nullable = true)
	private Float averageWait;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="lastPasswordCall", nullable = true)
	private Date lastPassordCall;
	

	public Date getLastPassordCall() {
		return lastPassordCall;
	}

	public void setLastPassordCall(Date lastPassordCall) {
		this.lastPassordCall = lastPassordCall;
	}

	public long getIdQueue() {
		return idQueue;
	}

	public void setIdQueue(long idQueue) {
		this.idQueue = idQueue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(Integer currentPassword) {
		this.currentPassword = currentPassword;
	}

	public Integer getLastPassword() {
		return lastPassword;
	}

	public void setLastPassword(Integer lastPassword) {
		this.lastPassword = lastPassword;
	}

	public Float getAverageWait() {
		return averageWait;
	}

	public void setAverageWait(Float averageWait) {
		this.averageWait = averageWait;
	}

}
