package br.com.queuemanager.queue.restservice;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.queuemanager.queue.entity.QueueEntity;
import br.com.queuemanager.queue.exception.QueueException;
import br.com.queuemanager.queue.service.QueueService;

@EnableWebMvc
@RestController
public class QueueRestService {
	
	@Inject
	private QueueService queueService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/findQueueByName")
	public @ResponseBody List<QueueEntity> findByName ( @RequestParam (value = "name") String name ){
		
		QueueEntity queueEntity = new QueueEntity();
		queueEntity.setName(name);
		
		try {
			return queueService.findByName(queueEntity);
		} catch (QueueException e) {
			List<QueueEntity> queueList = new ArrayList<QueueEntity>();
			queueEntity = new QueueEntity();
			queueEntity.setMessage(e.getMessage());			
			queueList.add(queueEntity);
			return queueList;			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/findQueueById")
	public @ResponseBody QueueEntity findByName ( @RequestParam (value = "idQueue") long idQueue ){
		
		QueueEntity queueEntity = new QueueEntity();
		queueEntity.setIdQueue(idQueue);
		
		try {
			return queueService.findById(queueEntity);
		} catch (QueueException e) {			
			queueEntity = new QueueEntity();
			queueEntity.setMessage(e.getMessage());			
			
			return queueEntity;			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/findQueueByType")
	public @ResponseBody List<QueueEntity> findByType( @RequestParam (value = "type") String type ){
		
		QueueEntity queueEntity = new QueueEntity();
		queueEntity.setName(type);
		
		try {
			return queueService.findByType(queueEntity);
		} catch (QueueException e) {
			List<QueueEntity> queueList = new ArrayList<QueueEntity>();
			queueEntity = new QueueEntity();
			queueEntity.setMessage(e.getMessage());			
			queueList.add(queueEntity);
			return queueList;			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/callNextPassword")
	public @ResponseBody QueueEntity callNextPassword( @RequestParam (value = "idQueue") long idQueue ){
		
		QueueEntity queueEntity = new QueueEntity();
		queueEntity.setIdQueue(idQueue);		
		try {			
			return queueEntity = queueService.callNextPassword(queueEntity); 			
		} catch (QueueException e) {
			queueEntity = new QueueEntity();
			queueEntity.setMessage(e.getMessage());			
			return queueEntity;			
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/requestPassword")
	public @ResponseBody QueueEntity requestPassword( @RequestParam (value = "idQueue") long idQueue ){
		
		QueueEntity queueEntity = new QueueEntity();
		queueEntity.setIdQueue(idQueue);		
		try {			
			return queueEntity = queueService.requestPassword(queueEntity);			
		} catch (QueueException e) {
			queueEntity = new QueueEntity();
			queueEntity.setMessage(e.getMessage());			
			return queueEntity;			
		}
	}

}
