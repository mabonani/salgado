package br.com.queuemanager.user.exception;

import br.com.exception.GenericException;

public class UserException extends GenericException{
	
	
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public UserException (){

	        super();
	    }

	    public UserException(String message, Throwable throwable) {

	        super(message, throwable);
	    }

	    public UserException(String message) {

	        super(message);
	    }

	    public UserException(Throwable throwable) {

	        super(throwable);
	    }

}
