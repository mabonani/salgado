package br.com.queuemanager.user.dao.impl;


import java.util.List;

import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import br.com.dao.impl.GenericDAOImpl;
import br.com.queuemanager.user.dao.UserDAO;
import br.com.queuemanager.user.entity.UserEntity;

@Named
public class UserDAOImpl extends GenericDAOImpl<UserEntity> implements UserDAO {

	
	public UserEntity login( UserEntity user) {
        List<UserEntity> userLogado;
        String query = "SELECT ue FROM userEntity ue"
					+ " WHERE LOWER(ue.login) = LOWER('"+ user.getLogin() + "') AND ue.senha='" + user.getPassword() +"')";	
        userLogado = super.findByQuery(query);

        if (userLogado.isEmpty()) {
            return null;
        } else {
            return userLogado.get(0);
        }
		
	}
	@Transactional
	public UserEntity save(UserEntity user ) throws org.springframework.dao.DataIntegrityViolationException {	
		
		return super.save(user);
		
	}
	
	public UserDAOImpl() {
		super(UserEntity.class);
	}

	@Override
	public UserEntity findById(UserEntity user) {
		 String query = "SELECT ue FROM userEntity ue"
					+ " WHERE LOWER(ue.login) = LOWER('"+ user.getLogin() + "')";
		 List<UserEntity> users = super.findByQuery(query);
		 
     if (users.isEmpty()) {
         return null;
     } else {
         return users.get(0);
     }
	}
	@Override
	public List<UserEntity> findAll() {
		
		return super.findALL(UserEntity.class.getName());	
		

	}		

	

}
