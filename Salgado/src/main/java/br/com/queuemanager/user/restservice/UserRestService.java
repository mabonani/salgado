package br.com.queuemanager.user.restservice;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.queuemanager.user.entity.UserEntity;
import br.com.queuemanager.user.exception.UserException;
import br.com.queuemanager.user.service.UserService;


@EnableWebMvc
@RestController
public class UserRestService {
	
	@Inject
	UserService userService;	

	@RequestMapping(method = RequestMethod.POST, value = "/addUser", headers="Accept=*/*")
	public @ResponseBody UserEntity addUser(
			@RequestParam(value = "name") String name,
			@RequestParam(value = "lastName") String lastName,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "login") String login,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "cpf") String cpf) {
	
		
		UserEntity userEntity = new UserEntity();
		
		userEntity.setName(name);
		userEntity.setLastName(lastName);
		userEntity.setEmail(email);
		userEntity.setLogin(login);
		userEntity.setPassword(password);
		userEntity.setCpf(cpf);
		
		try {
			userEntity = userService.save(userEntity);
		} catch (UserException e) {
			userEntity.setMessage(e.getMessage());
		}
		
		return userEntity;
		
		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getUsers")
	public @ResponseBody List<UserEntity> getUserList(){
		try {
			return userService.findAll();
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;	
    }		
		 
		 
		
		
		
	
	

}
