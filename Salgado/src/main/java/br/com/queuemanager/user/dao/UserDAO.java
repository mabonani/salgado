package br.com.queuemanager.user.dao;

import java.util.List;

import br.com.dao.GenericDAO;
import br.com.queuemanager.user.entity.UserEntity;

public interface UserDAO extends GenericDAO<UserEntity> {

    /**
     * method to find via jpql.
     * @param jpql jpql.
     * @param mapsParams map.
     */
    UserEntity login (UserEntity user);   
    UserEntity findById(UserEntity user);
    List<UserEntity> findAll();


}
