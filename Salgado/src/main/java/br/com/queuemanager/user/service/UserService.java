package br.com.queuemanager.user.service;

import java.util.List;

import br.com.queuemanager.user.entity.UserEntity;
import br.com.queuemanager.user.exception.UserException;

public interface UserService  {
	
	public UserEntity save(UserEntity user) throws UserException;
	public UserEntity find(UserEntity user);
	public UserEntity findById(UserEntity user) throws UserException;
	public void remove(UserEntity user);
	public boolean validaCadastro(UserEntity user) throws UserException;

    public UserEntity login(UserEntity user) throws UserException;
    
    public List<UserEntity> findAll () throws UserException;

}
