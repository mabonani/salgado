package br.com.queuemanager.user.service.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.queuemanager.user.dao.UserDAO;
import br.com.queuemanager.user.entity.UserEntity;
import br.com.queuemanager.user.exception.UserException;
import br.com.queuemanager.user.service.UserService;




@Named
public class UserServiceImpl  implements UserService{

	@Inject
	
    private UserDAO userDAO;		
   


    public UserEntity login(UserEntity userEntity) throws UserException  {
		  
		 UserEntity user = userDAO.login(userEntity);
		 
		 if (user == null){
            throw new UserException("Login ou senha invalidos!");
		 }
		 else
		 {
			 return user;
		 }
	}


    
	public UserEntity save(UserEntity user) throws UserException {
		
		try {
			user = userDAO.save(user);
		} catch (org.springframework.dao.DataIntegrityViolationException | SQLIntegrityConstraintViolationException e) {
			// TODO Auto-generated catch block
			user = null;
			throw new UserException("user ja Cadastrado!");		
		} 		
    	
    	 if (user == null){
             throw new UserException("Erro ao salvar user");             
 		 }
 		 else
 		 {
 			 return user;
 		 }
			
	}


   
	public UserEntity find(UserEntity user) {
		return userDAO.find(user);
	}



	public boolean userValidation(UserEntity user) throws UserException{
		if (user.getLogin().isEmpty()) {
			 throw new UserException("O login n�o pode ser nulo!");
		}
		return true;
		
	}



	public void remove(UserEntity user) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public UserEntity findById(UserEntity user)
			throws UserException {
		user = userDAO.findById(user);
		
		if (user == null){
			throw new UserException("user n�o encontrado!");
		}
		else{
			return user;
		}
	}



	@Override
	public List<UserEntity> findAll() throws UserException {
		List<UserEntity> users = userDAO.findAll();
		
		if (users == null){
			throw new UserException("Nenhum user encontrado!");
		}
		else{
			return users;
		}		
				
		
	}



	@Override
	public boolean validaCadastro(UserEntity user) throws UserException {
		// TODO Auto-generated method stub
		return false;
	}



}
