package br.com.queuemanager.menu.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.queuemanager.menu.dao.MenuDAO;
import br.com.queuemanager.menu.entity.MenuEntity;
import br.com.queuemanager.menu.exception.MenuException;
import br.com.queuemanager.menu.service.MenuService;

@Named
public class MenuServiceImpl implements MenuService{
	
	@Inject	
	private MenuDAO menuDAO;	
	
	public List<MenuEntity> findALL() throws MenuException{
		List<MenuEntity> listMenu = menuDAO.findALL();
		ArrayList<MenuEntity> x = (ArrayList<MenuEntity>) menuDAO.findALL();
		
		
		if (listMenu.size() == 0){
			throw new MenuException("N�o existem menus");
		}
		
		return listMenu;
	}	

	public List<MenuEntity> findByMenuFather(MenuEntity menuFather) {
		return menuDAO.findByMenuFather(menuFather);
	}
	
	public List<MenuEntity> findByMenuFather(MenuEntity menu, List<MenuEntity> listaMenu){
		List<MenuEntity> listWoNull = listaMenu.stream().filter(p -> p.getIdMenuFather() != null).collect(Collectors.toList());
		return listWoNull.stream().filter(p -> p.getIdMenuFather().intValue() == menu.getIdMenu() ).collect(Collectors.toList());
	}

	public boolean validateSubMenu(MenuEntity menuFather) {
		return menuDAO.validateSubMenu(menuFather);
	}
	
	public boolean validateSubMenu(MenuEntity m, List<MenuEntity> listMenu){
		List<MenuEntity> listWoNull = listMenu.stream().filter(p -> p.getIdMenuFather() != null).collect(Collectors.toList());
		return (listWoNull.stream().filter(p -> p.getIdMenuFather().intValue() == m.getIdMenu()).collect(Collectors.toList()).size() >0);		
	}

	@Override
	public MenuEntity save(MenuEntity genericEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MenuEntity saveOrUpdate(MenuEntity genericEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(MenuEntity genericEntity) {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
