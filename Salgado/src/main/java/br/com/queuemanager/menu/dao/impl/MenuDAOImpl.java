package br.com.queuemanager.menu.dao.impl;

import java.util.List;

import javax.inject.Named;

import br.com.dao.impl.GenericDAOImpl;
import br.com.queuemanager.menu.dao.MenuDAO;
import br.com.queuemanager.menu.entity.MenuEntity;

@Named
public class MenuDAOImpl extends GenericDAOImpl<MenuEntity> implements MenuDAO{	
	
	public List<MenuEntity> findALL() {		
		return super.findALL("MenuEntity");
	}

	public List<MenuEntity> findByMenuFather(MenuEntity menuFather) {
		    String query = "SELECT m FROM " + menuFather.getClass().getName() + " m"  
						+ " WHERE m.idMenuFather=" + menuFather.getIdMenu();
	        return findByQuery(query);
	
	}

	public int countByMenuFather(MenuEntity menuFather) {
		
		return findByMenuFather(menuFather).size();
		
	}

	public boolean validateSubMenu(MenuEntity menu) {
		return (countByMenuFather(menu) > 0);
	}
	
	
	

}
