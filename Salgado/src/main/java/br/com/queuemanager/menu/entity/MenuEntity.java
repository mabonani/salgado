package br.com.queuemanager.menu.entity;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.entity.GenericEntity;

@Entity
@Table(name = "MenuEntity")
public class MenuEntity extends GenericEntity { 

	private static final long serialVersionUID = 4493836566684475386L;
	
	@Id @GeneratedValue
	@Column(name="IdMenu", nullable = false)
	private int idMenu;
	
	@Column(name="description", nullable = false) 
	private String description;
	
	@Column(name="viewId", nullable = true) 
	private String viewId;
	
	@Column(name="IdMenuFather", nullable = true)
	private Integer idMenuFather;
	
	@ManyToOne(fetch = FetchType.LAZY)  
    @JoinColumn(name = "IdMenuFather", referencedColumnName = "IdMenu", updatable = false, insertable = false, nullable = true)  
	private MenuEntity menuFather;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "menuFather") 
	private List<MenuEntity> listMenu;

	public int getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String descricao) {
		this.description = descricao;
	}

	public String getViewId() {
		return viewId;
	}

	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

	public Integer getIdMenuFather() {
		return idMenuFather;
	}

	public void setIdMenuFather(Integer idMenuPai) {
		this.idMenuFather = idMenuPai;
	}

	public MenuEntity getMenuFather() {
		return menuFather;
	}

	public void setMenuFather(MenuEntity menuFather) {
		this.menuFather = menuFather;
	}

	public List<MenuEntity> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<MenuEntity> listMenu) {
		this.listMenu = listMenu;
	}
}