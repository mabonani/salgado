package br.com.queuemanager.menu.exception;

import br.com.exception.GenericException;

public class MenuException extends GenericException {
	
	private static final long serialVersionUID = 1L;

		public MenuException (){

	        super();
	    }

	    public MenuException(String message, Throwable throwable) {

	        super(message, throwable);
	    }

	    public MenuException(String message) {

	        super(message);
	    }

	    public MenuException(Throwable throwable) {

	        super(throwable);
	    }


}
