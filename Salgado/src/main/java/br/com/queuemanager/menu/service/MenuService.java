package br.com.queuemanager.menu.service;

import java.util.List;

import br.com.queuemanager.menu.entity.MenuEntity;
import br.com.queuemanager.menu.exception.MenuException;
import br.com.service.GenericService;

public interface MenuService extends GenericService<MenuEntity>{
	
	public List<MenuEntity> findALL() throws MenuException;
	public List<MenuEntity> findByMenuFather(MenuEntity menuFather);
	public List<MenuEntity> findByMenuFather(MenuEntity menu, List<MenuEntity> listMenu);
	public boolean validateSubMenu(MenuEntity menuFather);
	public boolean validateSubMenu(MenuEntity menu, List<MenuEntity> listMenu);

}
