package br.com.queuemanager.menu.dao;

import java.util.List;

import br.com.dao.GenericDAO;
import br.com.queuemanager.menu.entity.MenuEntity;

public interface MenuDAO extends GenericDAO<MenuEntity> {
	
	public List<MenuEntity> findALL();
	
	public List<MenuEntity> findByMenuFather(MenuEntity menuFather);	
	
	public boolean validateSubMenu(MenuEntity menu);
	
	public int countByMenuFather(MenuEntity menuFather);

}
