package br.com.queuemanager.queuetype.dao;

import java.util.List;

import br.com.dao.GenericDAO;
import br.com.queuemanager.queuetype.entity.QueueTypeEntity;

public interface QueueTypeDAO extends GenericDAO<QueueTypeEntity> {
	
	QueueTypeEntity findById(QueueTypeEntity queueTypeEntity);
	List<QueueTypeEntity> findByType(QueueTypeEntity queueTypeEntity);	
	List<QueueTypeEntity> findAll();
}
