package br.com.queuemanager.queuetype.exception;

import br.com.exception.GenericException;

public class QueueTypeException extends GenericException{	
	
	private static final long serialVersionUID = 8199340610430680335L;

	public QueueTypeException (){
	    super();
	}

    public QueueTypeException(String message, Throwable throwable) {

        super(message, throwable);
    }

    public QueueTypeException(String message) {

        super(message);
    }

    public QueueTypeException(Throwable throwable) {

        super(throwable);
    }

}
