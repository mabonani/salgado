package br.com.queuemanager.queuetype.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.entity.GenericEntity;

@Entity
@Table(name = "QueueTypeEntity")
public class QueueTypeEntity extends GenericEntity{

		private static final long serialVersionUID = 8813054247442801569L;
		
		@Id @GeneratedValue
		@Column(name="idTypeQueue", nullable = false)
		private long idTypeQueue;
		
		@Column(name="T", nullable = false)
		private long type;

		public long getIdTypeQueue() {
			return idTypeQueue;
		}

		public void setIdTypeQueue(long idTypeQueue) {
			this.idTypeQueue = idTypeQueue;
		}

		public long getType() {
			return type;
		}

		public void setType(long type) {
			this.type = type;
		}
		

}
