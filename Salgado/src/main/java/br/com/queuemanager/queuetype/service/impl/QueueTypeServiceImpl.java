package br.com.queuemanager.queuetype.service.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.queuemanager.queuetype.dao.QueueTypeDAO;
import br.com.queuemanager.queuetype.entity.QueueTypeEntity;
import br.com.queuemanager.queuetype.exception.QueueTypeException;
import br.com.queuemanager.queuetype.service.QueueTypeService;
import br.com.queuemanager.user.exception.UserException;

@Named
public class QueueTypeServiceImpl implements QueueTypeService {
	
	@Inject
	private QueueTypeDAO queueTypeDAO;

	@Override
	public QueueTypeEntity save(QueueTypeEntity queueTypeEntity)
			throws QueueTypeException {
		try {
			return queueTypeDAO.save(queueTypeEntity);
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new QueueTypeException("The type" + queueTypeEntity.getType() + " already exists");
			
		}
	}

	@Override
	public QueueTypeEntity saveOrUpdate(QueueTypeEntity queueTypeEntity)
			throws QueueTypeException {
		return queueTypeDAO.saveOrUpdate(queueTypeEntity);
	}

	@Override
	public void remove(QueueTypeEntity queueTypeEntity) {
		queueTypeDAO.remove(queueTypeEntity);		
	}

	@Override
	public QueueTypeEntity findById(QueueTypeEntity queueTypeEntity) throws QueueTypeException {
		queueTypeEntity = queueTypeDAO.findById(queueTypeEntity);
		
		if (queueTypeEntity == null) {
			throw new QueueTypeException("Type not found");
		}
		
		return queueTypeEntity;
	}

	@Override
	public List<QueueTypeEntity> findByType(QueueTypeEntity queueTypeEntity) throws QueueTypeException {
		List<QueueTypeEntity>  queueTypeList = queueTypeDAO.findByType(queueTypeEntity);
		
		if ( queueTypeList == null){
			throw new QueueTypeException("Type not found");
		}
		
		return queueTypeList;
	}

}
