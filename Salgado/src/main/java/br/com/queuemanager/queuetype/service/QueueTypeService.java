package br.com.queuemanager.queuetype.service;

import java.util.List;

import br.com.queuemanager.queuetype.entity.QueueTypeEntity;
import br.com.queuemanager.queuetype.exception.QueueTypeException;
import br.com.service.GenericService;

public interface QueueTypeService extends GenericService<QueueTypeEntity> {
	
	QueueTypeEntity findById(QueueTypeEntity queueTypeEntity) throws QueueTypeException;
	List<QueueTypeEntity> findByType(QueueTypeEntity queueTypeEntity) throws QueueTypeException;;

}
