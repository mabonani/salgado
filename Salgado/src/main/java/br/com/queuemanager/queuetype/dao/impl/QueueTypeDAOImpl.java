package br.com.queuemanager.queuetype.dao.impl;

import java.util.List;

import javax.inject.Named;

import br.com.dao.impl.GenericDAOImpl;
import br.com.queuemanager.queuetype.dao.QueueTypeDAO;
import br.com.queuemanager.queuetype.entity.QueueTypeEntity;

@Named
public class QueueTypeDAOImpl extends GenericDAOImpl<QueueTypeEntity> implements QueueTypeDAO {

	@Override
	public QueueTypeEntity findById(QueueTypeEntity queueTypeEntity) {
		return super.findById(queueTypeEntity,queueTypeEntity.getIdTypeQueue());
	}

	@Override
	public List<QueueTypeEntity> findByType(QueueTypeEntity queueTypeEntity) {
		String query = "SELECT qt FROM " + queueTypeEntity.getClass().getName() + 
					   " WHERE qt.type like '%" + queueTypeEntity.getType() +"%'";
		return super.findByQuery(query);		
	}
	
	public List<QueueTypeEntity> findAll (){
		return super.findALL(QueueTypeEntity.class.getName());
	}

}

